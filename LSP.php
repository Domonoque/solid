<?php


namespace Incorrect;

class Rectangle
{
    protected float $w;
    protected float $h;

    public function setW(float $w): void
    {
        $this->w = $w;
    }

    public function setH(float $h): void
    {
        $this->h = $h;
    }

    public function getW(): float
    {
        return $this->w;
    }

    public function getH(): float
    {
        return $this->h;
    }
}

function testSquare(Rectangle $rectangle): bool
{
    $rectangle->setW(5);
    $rectangle->setH(4);

    return ($rectangle->getH() * $rectangle->getW()) == 20;
}

var_dump(testSquare(new Rectangle()));

class Square extends Rectangle
{
    public function setW(float $w): void
    {
        $this->w = $w;
        $this->h = $w;

    }

    public function setH(float $h): void
    {
        $this->h = $h;
        $this->w = $h;
    }
}

var_dump(testSquare(new Square()));