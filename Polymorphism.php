<?php

namespace PHP73;

abstract class AbstractAnimal
{
    abstract public function say(string $text): string;
}

final class Cat extends AbstractAnimal
{
    public function say(string $text): string
    {
        return 'Cat say: ' . $text;
    }
}

//echo (new Cat)->say('meau');

//////////////////////////////////////////////

namespace PHP74;

interface SomeInterface
{
    public function interfaceMethod(): void;
}

abstract class AbstractClass
{
    abstract public function method(): SomeInterface;
}

final class SomeImplementedClass implements SomeInterface
{
    public function interfaceMethod(): void
    {
        // some code
    }
}

final class SomeClass extends AbstractClass
{
    public function method(): SomeImplementedClass
    {
        return new SomeImplementedClass();
    }
}

//(new SomeClass())->method();