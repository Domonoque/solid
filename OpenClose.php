<?php

namespace Incorrect;

use UnexpectedValueException;

abstract class SomeClass {};
final class ClassA extends SomeClass {};
final class ClassB extends SomeClass {};

final class StaticFactory
{
    public static function factory(int $type): SomeClass
    {
        if ($type === 1) {
            return new ClassA();
        } elseif ($type === 2) {
            return new ClassB();
        }

        throw new UnexpectedValueException();
    }
}

StaticFactory::factory(1);

namespace Correct;

use http\Exception\RuntimeException;
use UnexpectedValueException;

abstract class SomeClass {};
final class ClassA extends SomeClass {};
final class ClassB extends SomeClass {};

final class StaticFactory
{
    private static array $items = [];

    public static function setItems(array $items): void
    {
        self::$items = $items;
    }

    public static function factory(int $type): SomeClass
    {
        if (empty(self::$items)) {
            throw new RuntimeException();
        }

        if (!array_key_exists($type, self::$items)) {
            throw new UnexpectedValueException();
        }

        return resolve(self::$items[$type]);
    }
}

StaticFactory::setItems([
    1 => ClassA::class,
    2 => ClassB::class,
]);


function resolve($var): SomeClass {};
StaticFactory::factory(1);
