<?php

namespace Incorrect;

final class Order
{
    public iterable $products;
}

final class IncorrectUser
{
    private string $name = '';

    public function getName()
    {
        return $this->name;
    }

    public function calcOrder(Order $order): float
    {
        $sum = 0;

        foreach ($order->products as $product) {
            $sum += $product->price * $product->count;
        }

        return $sum;
    }
}

//(new IncorrectUser())->getName();
//(new IncorrectUser())->calcOrder(new Order());

namespace Correct;

final class Order
{
    private iterable $products;

    public function calc(): float
    {
        $sum = 0;

        foreach ($this->products as $product) {
            $sum += $product->price * $product->count;
        }

        return $sum;
    }
}

final class CorrectUser
{
    private string $name = '';

    public function getName()
    {
        return $this->name;
    }
}

//(new CorrectUser())->getName();
//(new Order())->calc();