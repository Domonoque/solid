<?php

namespace Incorrect;

final class Reader
{
    public function read()
    {
        global $argv;
        return $argv[1];
    }
}

final class Writer
{
    public function write(string $data)
    {
        echo $data;
    }
}

final class Copy
{
    public function make()
    {
        (new Writer)->write((new Reader())->read());
    }
}

//(new Copy)->make();

namespace Correct;

abstract class Reader
{
    abstract public function read(): string;
}

final class ConsoleReader extends Reader
{
    public function read(): string
    {
        global $argv;
        return $argv[1];
    }
}

abstract class Writer
{
    abstract function write(string $data): void;
}

final class ConsoleWriter extends Writer
{
    public function write(string $data): void
    {
        echo $data;
    }
}

final class Copy
{
    public function make(Reader $reader, Writer $writer)
    {
        $writer->write($reader->read());
    }
}

//(new Copy)->make(new ConsoleReader(), new ConsoleWriter());